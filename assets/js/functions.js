$(document).ready(function() {
  $('#fullpage').fullpage({
    navigation: true,
    anchors: ['home', 'whatwedo', 'why', 'google', 'footer'],
    css3: true
  });
});